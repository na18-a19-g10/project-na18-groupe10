-- affichier la liste de tous les usagers
CREATE VIEW VUsager AS
SELECT ID_utilisateur, Password, Nom,Prenom,
  DateNaissance, Sexe,Description, Email, 
  Credit, Adresse, NumeroCompte
FROM Utilisateur
WHERE type = 'Usager';

-- affichier la liste de tous les administrateurs
CREATE VIEW VAdministrateur AS
SELECT ID_utilisateur, Password, Nom,Prenom,
  DateNaissance, Sexe,Description, Email, 
  Credit, Adresse, NumeroCompte, NiveauDroit
FROM Utilisateur
WHERE type = 'Administrateur';


-- affichier la liste de tous les annonces d'offre
CREATE VIEW VOffre AS
SELECT * 
FROM Annonce a JOIN Offre o ON a.Nb_annonce = o.Nb_offre;


-- affichier la liste de tous les annonces de demande
CREATE VIEW VDemande AS
SELECT * 
FROM Annonce a JOIN Demande d ON a.Nb_annonce = d.Nb_demande;


-- affichier la liste de tous les annonces d'enchere
CREATE VIEW VEnchere AS
SELECT a.*, e.Prix_initial, e.Prix_Actuel, e.Gagneur, u.nom, u.prenom
FROM (Annonce a JOIN Enchere e ON a.Nb_Annonce = e.Nb_Enchere) 
JOIN Utilisateur u ON e.Gagneur = u.ID_Utilisateur;


-- affichier les informations de transtractions
CREATE VIEW VTranstraction AS
SELECT u.ID_utilisateur, u.nom, u.prenom, 
	t.Nb_Transtraction, t.Status, t.Temps_action, t.Commentaire, 
	a.Nb_Annonce, a.Title
FROM Utilisateur u, Transtraction t, Annonce a
WHERE u.ID_utilisateur = t.Usager AND t.Nb_Annonce = a.Nb_Annonce;


-- affichier les informations de chaque proposition
CREATE VIEW VProposition AS
SELECT u.Id_utilisateur, u.nom, u.prenom, p.Nb_Enchere, p.prix, e.Prix_Initial, e.Prix_Actuel
FROM Utilisateur u, Proposition p, Enchere e
WHERE u.Id_utilisateur = p.Id_utilisateur AND p.Nb_Enchere = e.Nb_Enchere;


-- affichier la liste d'utilisateur qui a peut proposer 

CREATE VIEW PouvoirPoposer AS
SELECT u.Id_utilisateur, u.nom, u.prenom, cm.Montant > p.prix AS pouvoir_proposer
FROM Utilisateur u, Compte_monetaire cm, Proposition p
WHERE u.NumeroCompte = cm.Nb_compte AND u.Id_utilisateur = p.Id_utilisateur;

-- affichier la liste d'utilisateur qui peut payer un offre
CREATE VIEW PouvoirPayerOffre AS
SELECT u.Id_utilisateur, u.nom, u.prenom, cm.Montant > o.prix AS pouvoir_payer
FROM Utilisateur u, Compte_monetaire cm, Offre o, Annonce a
WHERE u.NumeroCompte = cm.Nb_compte AND u.Id_utilisateur = a.Id_utilisateur AND o.nb_offre = a.Nb_Annonce;


-- affichier le prix proposé maximale de chaque enchere en tant que le prix actuel de chaque enchere
CREATE VIEW prix_actuel AS
SELECT nb_enchere, MAX (prix) AS prix_actuel
FROM proposition
GROUP BY nb_enchere;


-- affichier le nombre total de proposition par chaque utilisateur
--CREATE VIEW usager_nb_proposition AS

SELECT p.id_utilisateur, u.nom, u.prenom, COUNT(*) AS num_proposition
FROM proposition p, utilisateur u
WHERE p.id_utilisateur = u.id_utilisateur
GROUP BY p.id_utilisateur, u.nom, u.prenom
ORDER BY id_utilisateur;