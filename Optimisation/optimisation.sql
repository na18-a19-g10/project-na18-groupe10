
/* 1. Partition(1)*/
-- On utilise beaucoup de fois de ID_utilisateur et lee nom , prénom et 2 foreign keys dans la classe Utilisateur, pour les autres on n'utilise pas beaucoup
-- Donc on sépare la classe Utilsateur
CREATE TABLE UtilisateurMain(
	ID_utilisateur VARCHAR(10) PRIMARY KEY,
	Nom VARCHAR(50) NOT NULL, 
	Prenom VARCHAR(50) NOT NULL, 
	Adresse VARCHAR NOT NULL, 
	NumeroCompte VARCHAR,
	FOREIGN KEY (Adresse) REFERENCES Adresse (Nb_Adresse),
	FOREIGN KEY (NumeroCompte) REFERENCES Compte_monetaire (Nb_compte),
	UNIQUE (Nom,Prenom)
);

CREATE TABLE PersonneInfo(
	Password VARCHAR NOT NULL,
	DateNaissance Date, 
	Sexe CHAR(1), 
	Type VARCHAR NOT NULL, 
	Description text, 
	Email VARCHAR(50) PRIMARY KEY, 
	Credit INTEGER, 
	NiveauDroit INTEGER,
	ID_utilisateur VARCHAR(10) NOT NULL UNIQUE,
	FOREIGN KEY (ID_utilisateur) REFERENCES UtilisateurMain (ID_utilisateur),
	CHECK (NiveauDroit BETWEEN 1 AND 3),
	CHECK (Credit BETWEEN 1 AND 5),
	CHECK (Type in ('Usager','Administrateur')),
	UNIQUE (Email)
);

-- Choisir les Information
SELECT *
FROM UtilisateurMain
WHERE Nom = "Marie";



/* Partition(2) */
CREATE VIEW vAdmin AS SELECT * FROM utilisateur WHERE niveaudroit>=1;
CREATE VIEW vUsager AS SELECT * FROM utilisateur WHERE niveaudroit=NULL;


/* partition et indexation (3) */ 
--Affichez le Nb de utilisateur et le Nb de l'annonce des transactions qui sont été effectué avec succès trié par Nb de l'usager.
SELECT usager, Nb_Annonce
FROM Transtraction 
WHERE Status='Successful'
ORDER BY usager;

/* ici nous utilisons plus souvent les attributs 'usager' et 'nb_annonce', on peut donc proposer un partitionnement vertical afin d'isoler les attributs usager, Nb_Annonce et ainsi ne pas alourdir la requête avec les autres informations et ainsi gagner une projection.

On peut proposer aussi un partitionnement horizontal par Status.

Pour optimiser cette requête il est également utile de poser 2 index, sur l'usager et le Nb_Annonce, afin d'améliorer la recherche sur les noms ainsi que le tri. 
*/

-- La base aura donc un nouveau schéma, sur le modèle de celui présenté ci-dessous:

CREATE TABLE Transaction_succ(
    Nb_Transaction VARCHAR(10)PRIMARY KEY, 
    usager VARCHAR(10), 
    Nb_Annonce VARCHAR(10),
    FOREIGN KEY (Nb_Transaction) REFERENCES Transaction (Nb_Transaction),
    FOREIGN KEY (Usager) REFERENCES Utilisateur (ID_utilisateur),
    FOREIGN KEY (Nb_Annonce) REFERENCES Annonce (Nb_annonce),
    UNIQUE(Usager,Nb_Annonce)

); 
CREATE INDEX usager ON Transaction_succ (usager);
CREATE INDEX Nb_Annonce ON Transaction_succ (Nb_Annonce);

CREATE TABLE Transaction_annule (
    Nb_Transaction VARCHAR(10) PRIMARY KEY, 
    usager VARCHAR(10), 
    Nb_Annonce VARCHAR(10),
    FOREIGN KEY (Nb_Transaction) REFERENCES Transaction (Nb_Transaction),
    FOREIGN KEY (Usager) REFERENCES Utilisateur (ID_utilisateur),
    FOREIGN KEY (Nb_Annonce) REFERENCES Annonce (Nb_annonce),
    UNIQUE(Usager,Nb_Annonce)
); 
CREATE INDEX usager ON Transaction_annule (usager);
CREATE INDEX Nb_Annonce ON Transaction_annule (Nb_Annonce);

CREATE TABLE Transaction_refuse (
    Nb_Transaction VARCHAR(10) PRIMARY KEY, 
    usager VARCHAR(10), 
    Nb_Annonce VARCHAR(10),
    FOREIGN KEY (Nb_Transaction) REFERENCES Transaction (Nb_Transaction),
    FOREIGN KEY (Usager) REFERENCES Utilisateur (ID_utilisateur),
    FOREIGN KEY (Nb_Annonce) REFERENCES Annonce (Nb_annonce),
    UNIQUE(Usager,Nb_Annonce)
); 
CREATE INDEX usager ON Transaction_refuse(usager);
CREATE INDEX Nb_Annonce ON Transaction_refuse (Nb_Annonce);

CREATE TABLE Transtraction(
  Nb_Transtraction VARCHAR (10) PRIMARY KEY,
  Type VARCHAR(20),
  Status VARCHAR(20) NOT NULL,
  Description text,
  Temps_action TIMESTAMP NOT NULL,
  Commentaire text,
  CHECK (Type in ('dons', 'propositon', 'enchere','echange', 'vente')),
  CHECK (Status in ('Successful', 'canceled', 'rejected')),
);


/* Vue vTransaction = JOINTURE_NATURELLE (UNION (Transaction_succ, Transaction_annule, Transaction_refuse), Transaction)*/

--Réécrivez les requêtes avec le nouveau schéma de la base.
SELECT *
FROM Transaction_succ
ORDER BY usager;




/* 2. changer ordre du requête */
-- Afficher les infos de Utilisateur (Nom, prenom) et infos des annonce dont les status sont "accepted" et temps disponible < 4 
SELECT u.nom, u. prenom, a.*
FROM Utilisateur u JOIN annonce a ON a.Id_utilisateur = u.Id_utilisateur AND a.status = 'accepted' AND Temps_disponible < 4;
-- NOT --
SELECT u.nom, u. prenom, a.*
FROM Utilisateur u ,annonce a
WHERE a.Id_utilisateur = u.Id_utilisateur AND a.status = 'accepted' AND Temps_disponible < 4;

/* 3. vue matérialisée (1) */
-- Enchere qui sont entrain de faire
CREATE TABLE vEnchereENTRAIN(
	Nb_Enchere VARCHAR(10) PRIMARY KEY,
	Prix_initial FLOAT NOT NULL,
	Prix_Actuel FLOAT NOT NULL,
	Title VARCHAR(50) NOT NULL, 
	Type_activite VARCHAR(20) NOT NULL,
	Description text
);

-- Materialized view initialization
INSERT INTO vEnchereENTRAIN
SELECT e.Nb_Enchere, e.Prix_initial, e.Prix_Actuel, a.Title, a.Type_activite, a.Description
FROM Enchere e JOIN Annonce a ON e.Nb_Enchere = a.Nb_annonce 
WHERE e.Gagneur is NULL;



/* 3. vue matérialisée (2) */
-- Lister toutes les annonces qui ont expiré.
SELECT Nb_annonce, Title, Type_activite,Id_utilisateur
FROM Annonce
WHERE NOW() -Temps_post > Temps_disponible;
/*
On peut proposer un partitionnement vertical afin d'isoler les attributs Nb_Annonce et title.
On peut également optimiser cette requête grâce à une vue matérialisée, réactualisée par exemple chaque heur pour vérifier si un annonce est expiré.
*/

-- Un nouveau schéma
CREATE TABLE AnnonceMain(
    Nb_Annonce VARCHAR(10)PRIMARY KEY,
    Title VARCHAR(50) NOT NULL,
    Type_activite VARCHAR(20) NOT NULL,
    Id_utilisateur VARCHAR(10)NOT NULL,
    FOREIGN KEY (Id_utilisateur) REFERENCES Utilisateur(ID_utilisateur)
); 

CREATE INDEX Nb_Annonce ON AnnonceMain (Nb_Annonce);
CREATE INDEX Title ON AnnonceMain (Title);
CREATE INDEX Type_activite ON AnnonceMain (Type_activite);
CREATE INDEX Id_utilisateurON AnnonceMain (Id_utilisateur);

CREATE TABLE AnnonceInfo (
	Nb_annonce VARCHAR(10) PRIMARY KEY,  
	Description text, 
	Temps_post date NOT NULL, 
	Status VARCHAR(20), 
    Temps_disponible INTEGER NOT NULL,
	Rubrique VARCHAR(100) NOT NULL,
    Nb_adresse VARCHAR(10) NOT NULL,
	FOREIGN KEY (Rubrique) REFERENCES Rubrique(Titre),
	FOREIGN KEY (Nb_adresse) REFERENCES Adresse(Nb_adresse),
    FOREIGN KEY (Nb_annonce) REFERENCES AnnonceMain(Nb_annonce),
    CHECK (Type_activite in ('vente', 'don', 'echange','recherche')),
    CHECK (Status in ('accepted','waiting'))
);

-- Materialized view creation
CREATE VIEW vAnnonceExpire AS
SELECT Nb_Annonce AS annonce, Title AS Title, 
FROM Annonce
WHERE NOW() -Temps_post > Temps_disponible;


--Réécrivez les requêtes
SELECT * FROM vAnnonceExpire;


/* 3. vue matérialisée (3) */
explain analyze select id_utilisateur,nom,prenom,type,email from utilisateur;
CREATE VIEW vUtilisateur1 AS SELECT id_utilisateur,nom,prenom,type,email from utilisateur;
explain analyze select * from vUtilisateur1;

