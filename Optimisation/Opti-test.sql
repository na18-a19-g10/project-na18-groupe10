/* Creation de vues */
explain analyze select id_utilisateur,nom,prenom,type,email from utilisateur;
CREATE VIEW vUtilisateur1 AS SELECT id_utilisateur,nom,prenom,type,email from utilisateur;
explain analyze select * from vUtilisateur1;

/* Partition de tables */
CREATE VIEW vAdmin AS SELECT * FROM utilisateur WHERE niveaudroit>=1;
CREATE VIEW vUsager AS SELECT * FROM utilisateur WHERE niveaudroit=NULL;
