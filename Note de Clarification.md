# Note de Clarification du projet de plateforme d’échange à l’UTC

### I) Objets et attributs de la base de données :

----



1. **Utilisateur->usager**

   - ID utilisateur  *(PRIMARY KEY)*
   - Password
   - Nom NOT NULL
   - Prénom NOT NULL
   - Date de naissance
   - Sexe
   - Description personnelle 
   - Email
   - Crédit
   - Numéro de compte *(FOREIGN KEY)*
   - Numéro de l'Address *(FOREIGN KEY)*

   

2. **Utilisateur->usager**

   

3. **Utilisateur->administrateur** 

   - Niveau de droit

     

4. **Critique**

   - ID utilisateur *(FOREIGN KEY)*

   - Description 

   - Niveau de critique

     

5. **Compte monétaire**

   - Numéro de compte *(PRIMARY KEY)*

   - Montant

     

6. **Annonce**

   - Numéro de l'annonce  *(PRIMARY KEY)*

   - Titre

   - Type de l'annonce (demande ou offre)

   - Type de l'action (vente, don, échange...)

   - Description 

   - Temps proposé

   - Status (acceptée,  annulée ou en attente)

   - Temps de disponibilité 

   - Rubrique *(FOREIGN KEY)*

   - Address *(FOREIGN KEY)*

   - ID Utilisateur (éditeur) *(FOREIGN KEY)*

   - Code de produit *(FOREIGN KEY)*

   - Méthode: Temps_restant (Temps de disponibilité) = temps_actuel - Date du post - Temps_restant

     

7. **Annonce ->Offre**

   - Prix

     

8. **Annonce->Demande**

  

9. **Annonce->Enchère**

   - Numéro de l'enchère *(PRIMARY KEY)*
   - Vendeur ID  *(FOREIGN KEY)*
   - Date du post
   - Prix initial
   - Prix actuel
   - Temps de disponibilité 
   - Gagneur ID  *(FOREIGN KEY)*
   - Méthode: Temps_restant (Temps de disponibilité) = temps_actuel - Date du post - Temps_restant 

10. <b>Proposition</b>

   - Prix

   - Numéro de l' enchère *(FOREIGN KEY)*

   - Id user *(FOREIGN KEY)*

     

11. **Produit**

    - Code de produit *(PRIMARY KEY)*

    - Type (objet ou service )

    - Catégorie 

    - Nom 

    - Description 

    - Image

      

12. **Adresse** 

    - Numéro d' adresse

    - Pays

    - Ville

    - Code postale

    - rue et numéro

      

13. **Rubrique** 

    - Titre *(PRIMERY KEY)*

    - Description  

      

14. **Transactions ** 

    - Numéro de transaction *(PRIMARY KEY)*

    - Id usager*(FOREIGN KEY)*

    - Type de l'action (dons, proposition d'enchère, échange, vente, ...)

    - Status (effectué avec succès, annulé...)

    - Description

    - Temps d'action

    - Numéro de l'annonce *(FOREIGN KEY)*

    - Commentaire

      

15. **Chat**

    - Texte

    - Date du post

    - Id envoyeur  *(FOREIGN KEY)*

    - Id receveur  *(FOREIGN KEY)*

      





### II) Liste des contraintes :

-------------------------

Il existe bien sur un certain nombre de contraintes à mettre en place. Tout d’abord, il faudrait vérifier à l’entrée qu’il ne puisse pas y avoir d’informations aberrantes. Par exemple, un usager ne doit pas pouvoir, au moment de renseigner les informations de son profil, rentrer un âge de 500 ans. La catégorie des usagers n’aura également pas accès à la modification de certains attributs, notamment le montant de leur compte ou encore le prix de produits proposés par d’autres utilisateurs.

Il faudra également vérifier qu’un usager ne puisse pas faire un critique sur un usager duquel il n’a rien commandé au même titre qu’il ne pourra pas laisser un commentaire sur un produit qu’il n’a pas acheté.

On devra également vérifier qu’un produit ne puisse plus être acheté ou qu’une demande ne puisse plus recevoir de réponse une fois le délai de temps dépassé.

Des restrictions sur la cardinalité devront être mises en place, par exemple une annonce ne pourra pas être postée par deux utilisateurs, une demande ou une offre ne pourront être associées à deux temps et lieux. Cependant, deux annonces pourraient avoir les mêmes temps de commande et/ou les mêmes lieux. Cette situation est possible par exemple si un utilisateur propose plusieurs services pour une même période de temps.

Une autre restriction serait la possibilité par les usagers de modifier et de revenir sur leurs propres annonces et de pouvoir éditer les paramètres de ces dernières (temps, description, …).



### III) Utilisateurs et fonctions :

-----

Cette base de données devrait pouvoir permettre à deux catégories d’utilisateurs différentes d’accéder à des fonctionnalités différentes. La première est celle des usagers, qui pourront voir et poster des annonces pour des biens ou des services, réagir sur la qualité des services/biens fournis par des commentaires ou directement en postant des critiques sur la page de l’utilisateur ayant proposé l’offre. Ils pourront également communiquer entre eux via un système de chat pour des négociations sur des prix ou de plus amples informations sur un produit et finalement gérer un compte interne au système avec lequel ils pourront acheter, échanger et vendre des produits.

La deuxième catégorie d'utilisateurs  serait celle des administrateurs de la base de données. Ces derniers auront accès à plus d’informations que les usagers, notamment en matière de statistiques de produits. Ils pourront ainsi voir quelle rubrique est la plus populaire à un instant précis et ainsi la mettre en avant pour les usagers.  Ils pourront également voir quelles annonces sont sur le point de se terminer et les présenter en priorité.



### IV) Hypothèses :

----

Dans notre projet, nous avons décidé de différencier les offres et les demandes. Ainsi le fait de ne pas caractériser les demandes en objets ou en services comme nous l’avons fait pour les offres permet une flexibilité dans la réponse à une demande, quelqu’un qui souhaite répondre à une demande pourrait aussi bien proposer de prêter un objet qui permettrait de résoudre un problème que d’apporter son aide en direct en proposant un service. Dans le cas d’une offre cependant le problème ne se pose pas puisque l’on sait déjà ce que l’on propose.

###  

