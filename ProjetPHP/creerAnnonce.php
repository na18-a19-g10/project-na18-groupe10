<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Créer une annonce</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="homepage.php">Echange UTC</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                <a class="nav-link" href="homepage.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="creerAnnonce.php">creer Annonce</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="Mycompte.php">Mon Compte</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="MyAnnonce.php">Mon Annonce</a>
                </li>
                <!--<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
                </li>-->
                <li><?php if((isset($_COOKIE['username']))){
                    echo '<a href="login.php">Deconnexion</a>';
                    } else{
                        echo '<a href="login.php">Login</a>';
                      } 
                    
                    ?>
                    
                </li>
            </ul>
            <!--<form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>-->
            </div>
        </nav>
        <form class="form" action="creerAnnoncePHP.php" method="post">
            <div class="form-group">
                <label>Titre de l'annonce<span style="color:red"> *</span></label>
                <input type="text" class="form-control" name="annonceTitre" placeholder="Entrez la titre" />
            </div>
            <div class="form-group">
                <label>Type de l'annonce<span style="color:red"> *</span></label>
                <br />
                <input type="radio" id="annonceType_01" name="annonceType" value="vente" checked="checked" />Vente
                <input type="radio" id="annonceType_02" name="annonceType" value="don" />Don
                <input type="radio" id="annonceType_03" name="annonceType" value="echange" />Echange
                <input type="radio" id="annonceType_03" name="annonceType" value="recherche" />Recherche
            </div>
            <div class="form-group">
                <label>Description de l'annonce</label>
                <textarea class="form-control" id="annonceDes" name="annonceDes" placeholder="Description de l'annonce"></textarea>
            </div>
            <div class="form-group" id="Tempsdispo">
                <label>Temps disponible (en minutes)<span style="color:red"> *</span></label>
                <input type="number" min="0" step="1" value="0" class="form-control" id= "Tempsdispo" name= "Tempsdispo" />

            </div>
            <div class="form-group">
                <label>Rubrique<span style="color:red"> *</span></label>
                <select class="custom-select" name="rubriqueSelect">
                    <?php
                        include 'getId.php';
                        $vHost = 'tuxa.sme.utc';
                        $vPort = '5432';
                        $vDbname = 'dbbdd0a006';
                        $vUser = 'bdd0a006';
                        $vPassword = 'mZXw9KZk';
                        try{
                        $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vDbname", $vUser, $vPassword);
                        }
                        catch (Exception $e) {
                            echo 'Caught exception: ',  $e->POSTMessage(), "\n";
                        }
                        $vConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $vConn->beginTransaction();
                        $stmt = $vConn->prepare("SELECT titre FROM rubrique;");
                        $stmt->execute();
                        if($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                            echo "<option selected value = \"0\">Selectionnez la rubrique</option>";
                        }else{
                            echo "<option selected value = \"0\" disabled>Selectionnez la rubrique</option>";
                        }
                        while($row){
                            echo "<option value=".$row[titre].">".$row[titre]."</option>";
                            $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>OU créer un nouveau Rubrique<span style="color:red"> *</span></label>
                <input type="text" id="rubriqueInput" class="form-control" name="rubriqueInput" placeholder="entrez le rubrique">
                <label>Discription de la rubrique</label>
                <textarea class="form-control" id="rubriqueDes" name="rubriqueDes" placeholder="Description de la rubrique"></textarea>
            </div>
            <div class="form-group">
                <label>Mode de l'annonce<span style="color:red"> *</span></label>
                <br />
                <input  type="radio" id="annonceMode_01" name="annonceMode" value="0" checked="checked" onclick="enable()"/>Enchère
                <input type="radio" id="annonceMode_02" name="annonceMode" value="1" onclick="enable()" />Offre
                <input type="radio" id="annonceMode_03" name="annonceMode" value="2"  onclick="disable()"/>Demande
            </div>
            <div class="form-group" id="Prix">
                <label>Prix de l'annonce (€)<span style="color:red"> * Si vous n'avez pas chosit le Demande</span></label>
                <input type="number" min="0" step="0.01" value="0.00" class="form-control" id= "annoncePrix" name= "annoncePrix" />
            </div>
            <p>
                <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" style="color:aquamarine">
                    Ajoutez vortre produit
                </a>
            </p>
            <div class="collapse" id="collapseExample">
                <div class="form-group">
                    <label>Nom du produit</label>
                    <input type="text" id="nomProduit" class="form-control" name="nomProduit" placeholder="entrez le nom du produit">
                </div>
                 <div class="form-group">
                    <label>Type du produit</label>
                    <input  type="radio" id="typeProduit_01" name="typeProduit" value="objet" checked="checked" />Objet
                    <input type="radio" id="typeProduit_02" name="typeProduit" value="service" />Service
                </div>
                 <div class="form-group">
                    <label>Catégorie du produit</label>
                    <input type="text" id="catProduit" class="form-control" name="catProduit" placeholder="entrez la catégorie du produit">
                </div>
                 <div class="form-group">
                    <label>Photo du produit</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="photoProduit" name="photoProduit" accept="image/png, image/jpeg, image/gif, image/jpg"/>
                        <label class="custom-file-label" for="photoProduit">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Discription du produit</label>
                    <textarea class="form-control" id="disProduit" name="disProduit" placeholder="Description du produit"></textarea>
                </div>
            </div>
            <div class="form-group">
                    <button class="btn btn-primary" type="submit">Submit form</button>
            </div>
        </form>
        <script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.9.1/jquery.min.js">
            function disabled() {
                //document.getElementById("Prix").disabled = true; window.alert("***");
                $("#annoncePrix").attr("disabled",true);
            }
                function enabled() { document.getElementById("Prix").enabled = true; window.alert("****");}
                function showinfo(values){
                if(values==2){
                $("#annoncePrix").css("display","none");
                }
                else{
                $("#annoncePrix").css("display","block");
                }
            }
         </script>
    </div>
    

</body>
</html>