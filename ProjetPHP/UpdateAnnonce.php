
<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Créer une annonce</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="homepage.php">Echange UTC</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                <a class="nav-link" href="homepage.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="creerAnnonce.php">Creer Annonce</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="Mycompte.php">Mon Compte</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="MyAnnonce.php">Mon Annonce</a>
                </li>
                <!--<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
                </li>-->
                <li><?php if((isset($_COOKIE['username']))){
                    echo '<a href="login.php">Deconnexion</a>';
                    } else{
                        echo '<a href="login.php">Login</a>';
                      } 
                    
                    ?>
                    
                </li>
            </ul>
            <!--<form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>-->
            </div>
        </nav>
        <?php

        $id_annonce = $_GET['id'];
        $vHost = 'tuxa.sme.utc';
        $vPort = '5432';
        $vDbname = 'dbbdd0a006';
        $vUser = 'bdd0a006';
        $vPassword = 'mZXw9KZk';
        try{
        $vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vDbname", $vUser, $vPassword);
        //echo "connect";
        }
        catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        $username = "username";
        if(!isset($_COOKIE[$username])){
            echo '<script>alert("Connectez-vous, svp");location.href="login.html";</script>;';
        }
        else{
            $user_id = $_COOKIE[$username];
        }

        $sql="select * from annonce where nb_annonce = '". $id_annonce . "'";
        $res = $vConn->prepare($sql);
        $res->execute(); 

        echo "<br/><table class=\"table table-striped\">";

        echo "<tr class=\"thead-dark\">";
        echo "<th>Title</th>";
        echo "<th>Type d'activite</th>";
        echo "<th>Description</th>";
        echo "<th>Temps post</th>";
        echo "<th>Status</th>";
        echo "<th>Temps disponible</th>";
        echo "<th>Rubrique</th>";


        echo"</tr>";

        while($row = $res->fetch(PDO::FETCH_ASSOC)){ 

            echo "<tr>";
            echo "<td>$row[title]</td>";
            echo "<td>$row[type_activite]</td>";
            echo "<td>$row[description]</td>";
            echo "<td>$row[temps_post]</td>";
            echo "<td>$row[status]</td>";
            echo "<td>$row[temps_disponible]</td>";
            echo "<td>$row[rubrique]</td>";
            echo "</tr>";

        }
        echo "</table>";

        if($row["nb_produit"]){
            $nb_produit=$row["nb_produit"];
            $sql="select * from produit where nb_produit = '". $nb_produit . "'";
            $res = $vConn->prepare($sql);
            $res->execute(); 

            echo "<br/><table class=\"table table-striped\">";
            echo "<tr class=\"thead-dark\">";
            echo "<th>Nom</th>";
            echo "<th>Type</th>";
            echo "<th>Catégorie</th>";
            echo "<th>Description</th>";
            echo "<th>Image</th>";
            echo"</tr>";

            while($row = $res->fetch(PDO::FETCH_ASSOC)){ 

                echo "<tr>";
                echo "<td>$row[nomproduit]</td>";
                echo "<td>$row[typeproduit]</td>";
                echo "<td>$row[categorie]</td>";
                echo "<td>$row[description]</td>";
                echo "<td>$row[imagepath]</td>";
                echo "</tr>";

            }
            echo "</table>";
        }


        ?>


    <form method="post" action="doUpdateAnnonce.php?id=<?php echo $id_annonce ?>">

    <br/><br/><br/>title:<input type="text" name='title' value=<?php echo  $row["title"]?>><br/><br/>
    <label>Type de l'annonce</label>
                <br />
                <input type="radio" id="annonceType_01" name="annonceType" value="vente" checked="checked" />Vente
                <input type="radio" id="annonceType_02" name="annonceType" value="don" />Don
                <input type="radio" id="annonceType_03" name="annonceType" value="echange" />Echange
                <input type="radio" id="annonceType_03" name="annonceType" value="recherche" />Recherche
            <br/>
    Description:<input type="text" name='descrip' value=<?php echo  $row["description"]?>><br/><br/>
    Temps disponible:<input type="text" name='timeuseful' value=<?php echo  $row["temps_disponible"]?>><br/><br/>
   <!-- Rubrique:<input type="text" name='rubrique' value=<?php echo  $row["rubrique"]?>><br/><br/>-->
    <input type="submit" value="Submit" >

    </form>
    </div>
    </body>

</html>
