#!/usr/bin/python3

import psycopg2

HOST = "tuxa.sme.utc"
USER = "bdd0a006"
PASSWORD = "mZXw9KZk"
DATABASE = "dbbdd0a006"

# Open connection
conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))

# Open a cursor to send SQL commands
cur = conn.cursor()


### adresse
sql1 = '''INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A001', 'France', 'Lyon', '69000', 'Adresse 1');
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A002', 'France', 'Reims', '51100', 'Adresse 2');
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A003', 'France', 'Compiegne', '60200', 'Adresse 3');
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A004', 'France', 'Paris', '75002', 'Adresse 4');
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A005', 'France', 'Nice', '06100', 'Adresse 5');'''

cur.execute(sql1)
print("insert data into table Adresse successfully!!")

### Rubrique
sql2 = '''INSERT INTO Rubrique (Titre, Description) VALUES ('Equipement Auto','it is so good');
INSERT INTO Rubrique (Titre, Description) VALUES ('Cours particuliers','Des cours pour faore des fit, jouer des intruments...');
INSERT INTO Rubrique (Titre, Description) VALUES ('Hébergement','hébergement à vendre ou à louer...');
INSERT INTO Rubrique (Titre, Description) VALUES ('Table','Table à vendre...');
INSERT INTO Rubrique(titre, description)VALUES ('aide', 'aide!!s''il vous plait!');
INSERT INTO Rubrique(titre, description)VALUES ('Montre', 'montre à vente ou à enchere');
'''

cur.execute(sql2)
print("insert data into table Rubrique successfully!!")


### Utilisateur
sql3 = '''INSERT INTO Utilisateur (ID_utilisateur, Password, Nom, Prenom, DateNaissance, Sexe, Type, Description, Email, Credit, Adresse, NumeroCompte, NiveauDroit)
VALUES ('U00001','md23165E','Marie','Durand','1989-10-02','F','Usager','','Marie.Durand@gmail.com',3 ,'A001','C00001',NULL);
INSERT INTO Utilisateur (ID_utilisateur, Password, Nom, Prenom, DateNaissance, Sexe, Type, Description, Email, Credit, Adresse, NumeroCompte, NiveauDroit)
VALUES ('U00002','','Nathalie','Bernard','1980-07-16','F','Usager','','Nathalie.Bernard@gmail.com',3 ,'A005','C00002',NULL);
INSERT INTO Utilisateur (ID_utilisateur, Password, Nom, Prenom, DateNaissance, Sexe, Type, Description, Email, Credit, Adresse, NumeroCompte, NiveauDroit)
VALUES ('U00003','','Isabelle','Dubois','1995-02-05','F','Usager',' ','Isabelle.Dubois@gmail.com',3,'A004','C00003',NULL);
INSERT INTO Utilisateur (ID_utilisateur, Password, Nom, Prenom, DateNaissance, Sexe, Type, Description, Email, Credit, Adresse, NumeroCompte, NiveauDroit)
VALUES ('U00004','','Pierre','Thomas','1993-12-16','H','Usager',' ','Pierre.Thomas@gmail.com',2,'A002','C00004',NULL);
INSERT INTO Utilisateur (ID_utilisateur, Password, Nom, Prenom, DateNaissance, Sexe, Type, Description, Email, Credit, Adresse, NumeroCompte, NiveauDroit)
VALUES ('U00005','','Alain','Martin','1990-03-15','H','Administrateur',' ','Alain.Martin@gmail.com',5,'A003','C00005',1);
'''

cur.execute(sql3)
print("insert data into table Utilisateur successfully!!")


### Critique
sql4 = '''INSERT INTO Critique(ID_evaluateur,ID_evaluee, NiveauCritique, Description)
VALUES ('U00001','U00002',2,'Intégrité est Très haut...' );
INSERT INTO Critique(ID_evaluateur,ID_evaluee, NiveauCritique, Description)
VALUES ('U00005','U00002',3,'Trading à lheure');
INSERT INTO Critique(ID_evaluateur,ID_evaluee, NiveauCritique, Description)
VALUES ('U00003','U00001',4, '..');
INSERT INTO Critique(ID_evaluateur,ID_evaluee, NiveauCritique, Description)
VALUES ('U00003','U00004',4, '..');
INSERT INTO Critique(ID_evaluateur,ID_evaluee, NiveauCritique, Description)
VALUES ('U00001','U00004',2, '..');'''

cur.execute(sql4)
print("insert data into table Critique successfully!!")


### Annonce
sql5 = '''
INSERT INTO Annonce (Nb_annonce, Title, Type_activite, Description, Temps_post, Status, Temps_disponible, Rubrique, Id_utilisateur, Nb_adresse)
VALUES ('O001', 'Bonne verre vente', 'vente', 'With bonne verre， vous pouvez...', '2017-12-10', 'accepted', 3.2, 'Equipement Auto', 'U00001','A001');
INSERT INTO Annonce (Nb_annonce, Title, Type_activite, Description, Temps_post, Status, Temps_disponible, Rubrique, Id_utilisateur, Nb_adresse)
VALUES ('O002', 'Adaptateur secteur Usb', 'vente', 'Je vend un adaptateur secteur Usb, donc à brancher sur une prise, et de autre coté, il faut y brancher un câble USB, celui de votre téléphone.
C est un adaptateur récent, avec une bonne puissance, 3A, sur chaque sortie...', '2019-11-13', 'accepted', 4, 'Cours particuliers', 'U00002','A002');
INSERT INTO Annonce (Nb_annonce, Title, Type_activite, Description, Temps_post, Status, Temps_disponible, Rubrique, Id_utilisateur, Nb_adresse)
VALUES ('D001', 'Hébergement pour la MAker faire paris', 'recherche', 'bonjour a tous,je recherche un hébergement...', '2018-10-06', 'accepted', 5, 'Hébergement', 'U00005', 'A003');
INSERT INTO Annonce (Nb_annonce, Title, Type_activite, Description, Temps_post, Status, Temps_disponible, Rubrique, Id_utilisateur, Nb_adresse)
VALUES ('D002', 'Occuper de mon petit chat', 'recherche', 'bonjour a tous,je recherche une personne...', '2019-11-06', 'accepted',0, 'aide', 'U00005', 'A004');
INSERT INTO Annonce (Nb_annonce, Title, Type_activite, Description, Temps_post, Status, Temps_disponible, Rubrique, Id_utilisateur, Nb_adresse)
VALUES ('E001', 'Bonne armoire vente', 'vente', 'With bonne armoire， vous pouvez...', '2019-10-25', 'accepted', 5, 'Table', 'U00001', 'A005');
INSERT INTO Annonce (Nb_annonce, Title, Type_activite, Description, Temps_post, Status, Temps_disponible, Rubrique, Id_utilisateur, Nb_adresse)
VALUES ('E002', 'Bonne table vente', 'vente', 'With bonne table， vous pouvez...', '2019-10-11', 'accepted', 5, 'Table', 'U00004', 'A003');
INSERT INTO Annonce (Nb_annonce, Title, Type_activite, Description, Temps_post, Status, Temps_disponible, Rubrique, Id_utilisateur, Nb_adresse)
VALUES ('E003', 'Watch 1990', 'vente', 'Glycine - Collezione "AIRMAN WORLD TIME 4" Watch 1990 - Esaurito - 3323.35A - Homme - 1990', '2019-01-01', 'waiting', 10, 'Montre', 'U00004', 'A005');

'''

cur.execute(sql5)
print("insert data into table Annonce successfully!!")



### Offre
sql6 = '''
INSERT INTO Offre (nb_offre, prix)
VALUES ('O001',20);
INSERT INTO Offre (Nb_offre, prix)
VALUES ('O002',10);
'''
cur.execute(sql6)
print("insert data into table Offre successfully!!")

### Demande
sql7 = '''
INSERT INTO Demande (Nb_demande)
VALUES ('D001');
INSERT INTO Demande (Nb_demande)
VALUES ('D002');
'''
cur.execute(sql7)
print("insert data into table Demande successfully!!")

### Enchere
sql8 = '''
INSERT INTO Enchere (Nb_Enchere, Prix_initial, Prix_Actuel, Gagneur)
VALUES ('E001', 150, 160, 'U00001');
INSERT INTO Enchere (Nb_Enchere, Prix_initial, Prix_Actuel, Gagneur)
VALUES ('E002', 50, 100, 'U00004');
INSERT INTO Enchere (Nb_Enchere, Prix_initial, Prix_Actuel, Gagneur)
VALUES ('E003', 90, 120, NULL);
'''
cur.execute(sql8)
print("insert data into table Enchere successfully!!")

### Transtraction
sql9 = '''
INSERT INTO transaction(Nb_Transtraction, Usager, Nb_Annonce, Type, Status, Description, Temps_action, Commentaire)
VALUES ('T001', 'U00003', 'O002', 'vente', 'Successful', 'Un verre', '2019-10-01 00:00:01', 'So good');
INSERT INTO transaction(Nb_Transtraction, Usager, Nb_Annonce, Type, Status, Description, Temps_action, Commentaire)
VALUES ('T002', 'U00001', 'D001', 'vente', 'Successful', 'Un table', '2019-10-05 00:17:01', 'Nice');
'''
cur.execute(sql9)
print("insert data into table Transtraction successfully!!")

### Chat
sql10 = '''
INSERT INTO Chat(Id_envoyeur, Id_receveur, texte, temps_post)
VALUES ('U00001', 'U00002', 'xxx.xxx', '2019-10-01 00:00:00');
INSERT INTO Chat(Id_envoyeur, Id_receveur, texte, temps_post)
VALUES ('U00002', 'U00003', 'xxx.xxx', '2019-10-05 10:08:00');
INSERT INTO Chat(Id_envoyeur, Id_receveur, texte, temps_post)
VALUES ('U00002', 'U00004', 'xxx.xxx', '2019-10-06 20:05:00');
'''
cur.execute(sql10)
print("insert data into table Chat successfully!!")

### Produit
sql11 = '''
INSERT INTO Produit(Nb_Produit, typeProduit, categorie, NomProduit, Description, Imagepath, Annonce)
VALUES ('P001','objet','verre','Verre','','', 'O001');
INSERT INTO Produit(Nb_Produit, typeProduit, categorie, NomProduit, Description, Imagepath, Annonce)
VALUES ('P002','objet','Appareil','Adaptateur secteur Usb','un adaptateur récent, avec une bonne puissance, 3A, sur chaque sortie.','', 'O002');
INSERT INTO Produit(Nb_Produit, typeProduit, categorie, NomProduit, Description, Imagepath, Annonce)
VALUES ('P003','service','héberge','Hébergement pour la MAker faire paris','','', 'D001');
INSERT INTO Produit(Nb_Produit, typeProduit, categorie, NomProduit, Description, Imagepath, Annonce)
VALUES ('P004','objet','armoire','Armoire pour ranger','','', 'E001');
INSERT INTO Produit(Nb_Produit, typeProduit, categorie, NomProduit, Description, Imagepath, Annonce)
VALUES ('P005','objet','table','Table pour etudier','','', 'E002');
'''
cur.execute(sql11)
print("insert data into table Produit successfully!!")

### Proposition
sql12 = '''
INSERT INTO Proposition(Nb_enchere, Id_utilisateur, Prix) VAlUES ('E001', 'U00001', 160);
INSERT INTO Proposition(Nb_enchere, Id_utilisateur, Prix) VAlUES ('E001', 'U00002', 155);
INSERT INTO Proposition(Nb_enchere, Id_utilisateur, Prix) VAlUES ('E002', 'U00004', 100);
INSERT INTO Proposition(Nb_enchere, Id_utilisateur, Prix) VAlUES ('E002', 'U00003', 60);

INSERT INTO Proposition(Nb_enchere, Id_utilisateur, Prix) VAlUES ('E002', 'U00002', 85);
INSERT INTO Proposition(Nb_enchere, Id_utilisateur, Prix) VAlUES ('E003', 'U00002', 100);
INSERT INTO Proposition(Nb_enchere, Id_utilisateur, Prix) VAlUES ('E003', 'U00001', 120);
'''
cur.execute(sql12)
print("insert data into table Proposition successfully!!")

### Adresse
sql13 = '''
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A001', 'France', 'Lyon', '69000', 'Adresse 1');
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A002', 'France', 'Reims', '51100', 'Adresse 2');
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A003', 'France', 'Compiegne', '60200', 'Adresse 3');
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A004', 'France', 'Paris', '75002', 'Adresse 4');
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A005', 'France', 'Nice', '06100', 'Adresse 5');

'''
cur.execute(sql13)
print("insert data into table Adresse successfully!!")

# Commit (transactionnal mode is by default)
conn.commit()

# Close connection
conn.close()