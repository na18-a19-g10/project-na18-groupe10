#!/usr/bin/python3

import psycopg2

HOST = "tuxa.sme.utc"
USER = "bdd0a006"
PASSWORD = "mZXw9KZk"
DATABASE = "dbbdd0a006"

# Connect to an existing database
conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
if (conn):
    print("Connect to database successfully")
else:
    print("Can/'t onnect to database")

cur = conn.cursor()

### Adresse
sql1 = '''CREATE TABLE Adresse(Nb_Adresse VARCHAR(10) PRIMARY KEY,
  pays VARCHAR(10) NOT NULL, ville VARCHAR(20) NOT NULL,
  codePostal VARCHAR(5) NOT NULL,
  adresse text NOT NULL,
  UNIQUE (pays,ville,adresse,codePostal)
);'''
cur.execute(sql1)
print("create the table adresse successfully !")
## try:
#  cur.execute(sql1)
# except:
# print("Failed to create the table adresse")

### Utilisateur
sql2 = '''CREATE TABLE Utilisateur (
  ID_utilisateur VARCHAR(10) PRIMARY KEY,
  Password VARCHAR NOT NULL,
  Nom VARCHAR(50) NOT NULL, 
  Prenom VARCHAR(50) NOT NULL, 
  DateNaissance Date, 
  Sexe CHAR(1), 
  Type VARCHAR NOT NULL, 
  Description text, 
  Email VARCHAR(50) NOT NULL, 
  Credit INTEGER, 
  Adresse VARCHAR NOT NULL, 
  NumeroCompte VARCHAR NOT NULL,
  NiveauDroit INTEGER,      
  FOREIGN KEY (Adresse) REFERENCES Adresse (Nb_Adresse),
  FOREIGN KEY (NumeroCompte) REFERENCES Compte_monetaire (Nb_compte),
  CHECK (NiveauDroit BETWEEN 1 AND 3),
  CHECK (Credit BETWEEN 1 AND 5),
  CHECK (Type in ('Usager','Administrateur')),
  UNIQUE (Email),
  UNIQUE (Nom,Prenom)
) ; '''

cur.execute(sql2)
print("create the table utilisateursuccessfully !")

### Critique
sql3 = '''CREATE TABLE Critique (
    ID_evaluateur VARCHAR(10) ,
    ID_evaluee VARCHAR(10) ,
    NiveauCritique INTEGER,
    Description text,
    FOREIGN KEY (ID_evaluateur) REFERENCES Utilisateur(ID_utilisateur),
    FOREIGN KEY (ID_evaluee) REFERENCES Utilisateur (ID_utilisateur),
    PRIMARY KEY (ID_evaluateur,ID_evaluee),
    CHECK (ID_evaluateur <> ID_evaluee),
    CHECK (NiveauCritique BETWEEN 1 AND 5)
) ;'''
cur.execute(sql3)
print("create the table Critique successfully !")

### Chat
sql4 = '''CREATE TABLE Chat(
  Id_envoyeur VARCHAR(10),
  Id_receveur VARCHAR(10) ,
  texte text,
  temps_post Date,
  FOREIGN KEY (Id_envoyeur) REFERENCES Utilisateur (ID_utilisateur),
  FOREIGN KEY (Id_receveur) REFERENCES Utilisateur (ID_utilisateur),
  PRIMARY KEY(Id_envoyeur,Id_receveur),
  CHECK (Id_envoyeur <> Id_receveur)
);'''
cur.execute(sql4)
print("create the table Chat successfully !")

### Rubrique
sql5 = '''CREATE TABLE Rubrique (
  Titre VARCHAR(100) PRIMARY KEY,
  Description text
);
'''
cur.execute(sql5)
print("create the table Rubrique successfully !")

### Annonce
sql6 = '''CREATE TABLE Annonce ( Nb_annonce VARCHAR(10) PRIMARY KEY, 
    Title VARCHAR(50) NOT NULL, 
    Type_activite VARCHAR(20) NOT NULL,
    Description text, 
    Temps_post date NOT NULL, 
    Status VARCHAR(20), 
    Temps_disponible INTEGER NOT NULL,
    Rubrique VARCHAR(100) NOT NULL,
    Id_utilisateur VARCHAR(10)NOT NULL,
    Nb_adresse VARCHAR(10) NOT NULL,
    FOREIGN KEY (Rubrique) REFERENCES Rubrique(Titre),
    FOREIGN KEY (Id_utilisateur) REFERENCES Utilisateur(ID_utilisateur), 
    FOREIGN KEY (Nb_adresse) REFERENCES Adresse(Nb_adresse),
    CHECK (Type_activite in ('vente', 'don', 'echange','recherche')),
    CHECK (Status in ('accepted','waiting'))
);
'''
cur.execute(sql6)
print("create the table Annonce successfully !")

### Transaction
sql7 = '''CREATE TABLE Transaction(
  Nb_Transtraction VARCHAR (10) PRIMARY KEY,
  Usager VARCHAR (10) NOT NULL,
  Nb_Annonce VARCHAR (10) NOT NULL,
  Type VARCHAR(20),
  Status VARCHAR(20) NOT NULL,
  Description text,
  Temps_action TIMESTAMP NOT NULL,
  Commentaire text,
  FOREIGN KEY (Usager) REFERENCES Utilisateur (ID_utilisateur),
  FOREIGN KEY (Nb_Annonce) REFERENCES Annonce (Nb_annonce),
  CHECK (Type in ('dons', 'propositon', 'enchere','echange', 'vente')),
  CHECK (Status in ('Successful', 'canceled', 'rejected')),
  UNIQUE(Usager,Nb_Annonce)
);
'''
cur.execute(sql7)
print("create the table Transaction successfully !")


### Enchere
sql8 = '''CREATE TABLE Enchere (
  Nb_Enchere VARCHAR(10) PRIMARY KEY,
  Prix_initial FLOAT NOT NULL,
  Prix_Actuel FLOAT NOT NULL,
  Gagneur VARCHAR (10),
  FOREIGN KEY (Nb_Enchere) REFERENCES Annonce(Nb_annonce),
  FOREIGN KEY (Gagneur) REFERENCES Utilisateur(ID_utilisateur),
  CHECK (Prix_initial >=0),
  CHECK (Prix_Actuel >=0),
  CHECK (Prix_Actuel >= Prix_Initial)
);
'''
cur.execute(sql8)
print("create the table Enchere successfully !")

### Offre
sql9 = '''CREATE TABLE Offre(
  Nb_offre VARCHAR(10)PRIMARY KEY,
  prix FLOAT NOT NULL,
  FOREIGN KEY ( Nb_offre) REFERENCES Annonce(Nb_annonce),
  CHECK (prix >= 0)
);
'''
cur.execute(sql9)
print("create the table Offre successfully !")


### Demande
sql10 = '''CREATE TABLE Demande(
  Nb_demande VARCHAR(10) PRIMARY KEY,
  FOREIGN KEY (Nb_demande) REFERENCES Annonce(Nb_annonce) 
);
'''
cur.execute(sql10)
print("create the table Demande successfully !")


### Produit
sql11 = '''CREATE TABLE Produit(
  Nb_Produit VARCHAR(10) PRIMARY KEY,
  typeProduit VARCHAR,
  categorie VARCHAR,
  NomProduit VARCHAR NOT NULL,
  Description VARCHAR,
  Imagepath VARCHAR,
  Annonce VARCHAR(10) NOT NULL,
  FOREIGN KEY (Annonce) REFERENCES Annonce(Nb_annonce),
  CHECK (typeProduit in ('objet', 'service'))
);
'''
cur.execute(sql11)
print("create the table Produit successfully !")


### Proposition
sql12 = '''CREATE TABLE Proposition(
  Nb_enchere VARCHAR(10) ,
  Id_utilisateur VARCHAR(10) ,
  Prix FLOAT NOT NULL,
  FOREIGN KEY (Nb_enchere) REFERENCES Enchere(Nb_Enchere),
  FOREIGN KEY (Id_utilisateur) REFERENCES Utilisateur(Id_utilisateur),
  PRIMARY KEY(Nb_enchere,Id_utilisateur),
  CHECK (Prix >=0)
);

'''
cur.execute(sql12)
print("create the table Proposition successfully !")

### Compte_monetaire
sql13 = '''CREATE TABLE Compte_monetaire(
    Nb_compte VARCHAR(20) PRIMARY KEY, 
    Montant FLOAT CHECK(Montant >= 0)
);
'''
cur.execute(sql13)
print("create the table Compte_monetaire successfully !")

conn.commit()
# Close connection
conn.close()
