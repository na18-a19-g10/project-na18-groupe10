-- Utilisateur 1 a créé un login 
BEGIN TRANSACTION
INSERT INTO Utilisateur (ID_utilisateur, Password, Nom, Prenom, DateNaissance, Sexe, Type, Description, Email, Credit, Adresse, NumeroCompte, NiveauDroit)
VALUES ('U00001','md23165E','Marie','Durand','1989-10-02','F','Usager','','Marie.Durand@gmail.com',3 ,'A001','C00001',NULL);
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A001', 'France', 'Lyon', '69000', 'Adresse 1');
INSERT INTO Compte_monetaire (Nb_compte,Montant) VALUES ('C00001',0);
COMMIT;
/* Les trois opérations précédentes doivent toutes être exécutées, ou bien aucune, car en vertu des cardinalités du MCD :
-il ne peut exister d'utilisateur sans Compte monetaire,
-il ne peut exister d'utilisateur sans adresse,
La transaction assure l'unité logique de traitement de ces opérations.
*/

-- Utilisateur 2 créer son login et il dépose tout de suite 1000
INSERT INTO Utilisateur (ID_utilisateur, Password, Nom, Prenom, DateNaissance, Sexe, Type, Description, Email, Credit, Adresse, NumeroCompte, NiveauDroit)
VALUES ('U00002','','Nathalie','Bernard','1980-07-16','F','Usager','','Nathalie.Bernard@gmail.com',3 ,'A005','C00002',NULL);
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A005', 'France', 'Nice', '06100', 'Adresse 5');
INSERT INTO Compte_monetaire (Nb_compte,Montant) VALUES ('C00002',0);

UPDATE Compte_monetaire 
set Montant = Montant+1000
WHERE Nb_compte='C00002';

COMMIT;
/* Les quatre opérations précédentes correspondent à l'initialisation du utilisateur 2, il est donc conseillé qu'elles s'effectuent également de façon unitaire.
*/

-- utilisateur 2 a reçu le paiement d'autre usager qui a acheté leur produi.
BEGIN TRANSACTION
UPDATE Compte_monetaire
SET Montant = Montant + 120
WHERE Nb_compte = 'C00002';
COMMIT;
-- cette opération doit être séparé avec les opérations au-dessus.

-- utilisateur 1 a evalué utilisateur 2 et lui proposé un niveau de critique de 2
BEGIN TRANSACTION
INSERT INTO Critique(ID_evaluateur,ID_evaluee, NiveauCritique, Description)
VALUES ('U00001','U00002',2,'Intégrité est Très haut...' );
COMMIT;
-- utilisateur 1 a changé leur evaluation de niveau 2 à 0;
BEGIN TRANSACTION
UPDATE Critique
SET NiveauCritique = NiveauCritique - 2
WHERE ID_evaluateur = 'U00001' AND ID_evaluee = 'U00002';
COMMIT;
-- utilisateur 1 a changé leur evaluation de niveau 0 à 4;
BEGIN TRANSACTION
UPDATE Critique
SET NiveauCritique = NiveauCritique + 4
WHERE ID_evaluateur = 'U00001' AND ID_evaluee = 'U00002';
COMMIT;
-- Les 2 UPDATE na peut pas être fait au même temps, car s'ils faisons parallèle, le NiveauCritique peut-être dépasse le valeur maximum 5



-- utilisateur 1 a créé un annonce d'offre
BEGIN TRANSACTION
INSERT INTO Annonce (Nb_annonce, Title, Type_activite, Description, Temps_post, Status, Temps_disponible, Rubrique, Id_utilisateur, Nb_adresse) 
VALUES ('O001', 'Bonne verre vente', 'vente', 'With bonne verre， vous pouvez...', '2017-12-10', 'accepted', 3.2, 'Equipement Auto', 'U00001','A001');
INSERT INTO Offre (nb_offre, prix)
VALUES ('O001',20);
INSERT INTO Produit(Nb_Produit, typeProduit, categorie, NomProduit, Description, Imagepath, Annonce) 
VALUES ('P001','objet','verre','Verre','','', 'O001');
COMMIT;
/* Annonce et ses héritage doit être créés dans le même temps*/



-- utilisateur 2 a créé un enchère
-- enchere
BEGIN TRANSACTION
INSERT INTO Annonce (Nb_annonce, Title, Type_activite, Description, Temps_post, Status, Temps_disponible, Rubrique, Id_utilisateur, Nb_adresse) 
VALUES ('E001', 'Bonne armoire vente', 'vente', 'With bonne armoire， vous pouvez...', '2019-10-25', 'accepted', 5, 'Table', 'U00002', 'A005');
INSERT INTO Enchere (Nb_Enchere, Prix_initial, Prix_Actuel, Gagneur)
VALUES ('E001', 150, 150, '');
COMMIT;

--proposition
-- On suppose que utilisateur 2 a proposé un prix pour l'enchère E001
BEGIN TRANSACTION
INSERT INTO Proposition(Nb_enchere, Id_utilisateur, Prix) VAlUES ('E001', 'U00001', 160);
UPDATE Enchere 
SET prix_actuel = 160
WHERE Nb_Enchere = 'E001';
COMMIT;
/* Les deux options précédentes doivent toutes être exécutées, ou bien aucune, car quand une proposition est créé, le prix-actuel dans la relation enchère doit être update au même temps */
-- utilisateur 3 a proposé un prix pour l'enchère E001
BEGIN TRANSACTION
INSERT INTO Proposition(Nb_enchere, Id_utilisateur, Prix) VAlUES ('E001', 'U00003', 170);
UPDATE Enchere 
SET prix_actuel = 160
WHERE Nb_Enchere = 'E001';
COMMIT;

/*
Soit l'exécution concurrente des deux transactions ci-dessus:
La transaction 1 doit obtenir un verrou X quand elle exécute.
La transaction 2 est mise en attente avant son INSERT, car elle ne peut obtenir de verrou X déjà posé par la transaction 1. Mais dès que la transaction 1 exécute son COMMIT, la transaction 2 obtient de poser un verrou X (libéré par la transaction 1) et peut donc exécuter son INSERT. 
*/

-- Utilisateur 1 a créé un chat avec utilisateur 2
BEGIN TRANSACTION
INSERT INTO Chat(Id_envoyeur, Id_receveur, texte, temps_post)
VALUES ('U00001', 'U00002', 'Bonjour,', '2019-10-01 00:00:00');
COMMIT;

-- Il veut changé le texte qu'il a été envoyé. Et il ajoute ensuite un autre texte.
BEGIN TRANSACTION
UPDATE Chat
SET texte = 'NBA'
WHERE Id_envoyeur = 'U00001' AND Id_receveur = 'U00002' AND temps_post = '2019-10-01 00:00:00';
COMMIT;

-- Si les deux opérations sont fait au meme temps, il y aura des erreurs.