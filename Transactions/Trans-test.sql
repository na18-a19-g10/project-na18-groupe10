
BEGIN TRANSACTION;
INSERT INTO Utilisateur (ID_utilisateur, Password, Nom, Prenom, DateNaissance, Sexe, Type, Description, Email, Credit, Adresse, NumeroCompte, NiveauDroit)
VALUES ('U00001','md23165E','Marie','Durand','1989-10-02','F','Usager','','Marie.Durand@gmail.com',3 ,'A001','C00001',NULL);
INSERT INTO Utilisateur (ID_utilisateur, Password, Nom, Prenom, DateNaissance, Sexe, Type, Description, Email, Credit, Adresse, NumeroCompte, NiveauDroit)
VALUES ('U00002','','Nathalie','Bernard','1980-07-16','F','Usager','','Nathalie.Bernard@gmail.com',3 ,'A005','C00002',NULL);
INSERT INTO Utilisateur (ID_utilisateur, Password, Nom, Prenom, DateNaissance, Sexe, Type, Description, Email, Credit, Adresse, NumeroCompte, NiveauDroit)
VALUES ('U00003','','Isabelle','Dubois','1995-02-05','F','Usager',' ','Isabelle.Dubois@gmail.com',3,'A004','C00003',NULL);
INSERT INTO Utilisateur (ID_utilisateur, Password, Nom, Prenom, DateNaissance, Sexe, Type, Description, Email, Credit, Adresse, NumeroCompte, NiveauDroit)
VALUES ('U00004','','Pierre','Thomas','1993-12-16','H','Usager',' ','Pierre.Thomas@gmail.com',2,'A002','C00004',NULL);
INSERT INTO Utilisateur (ID_utilisateur, Password, Nom, Prenom, DateNaissance, Sexe, Type, Description, Email, Credit, Adresse, NumeroCompte, NiveauDroit)
VALUES ('U00005','','Alain','Martin','1990-03-15','H','Administrateur',' ','Alain.Martin@gmail.com',5,'A003','C00005',1);
COMMIT;

BEGIN TRANSACTION;
INSERT INTO Compte_monetaire (Nb_compte,Montant) VALUES ('C00001',120.2);
INSERT INTO Compte_monetaire (Nb_compte,Montant) VALUES ('C00002',56.0);
INSERT INTO Compte_monetaire (Nb_compte,Montant) VALUES ('C00003',189.34);
INSERT INTO Compte_monetaire (Nb_compte,Montant) VALUES ('C00004',87.08);
INSERT INTO Compte_monetaire (Nb_compte,Montant) VALUES ('C00005',43.45);
COMMIT;

BEGIN TRANSACTION;
INSERT INTO Critique(ID_evaluateur,ID_evaluee, NiveauCritique, Description)
VALUES ('U00001','U00002',2,'Intégrité est Très haut...' );
INSERT INTO Critique(ID_evaluateur,ID_evaluee, NiveauCritique, Description)
VALUES ('U00005','U00002',3,'Trading à lheure');
INSERT INTO Critique(ID_evaluateur,ID_evaluee, NiveauCritique, Description)
VALUES ('U00003','U00001',4, '..');
INSERT INTO Critique(ID_evaluateur,ID_evaluee, NiveauCritique, Description)
VALUES ('U00003','U00004',4, '..');
INSERT INTO Critique(ID_evaluateur,ID_evaluee, NiveauCritique, Description)
VALUES ('U00001','U00004',2, '..');
COMMIT;

BEGIN TRANSACTION;
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A001', 'France', 'Lyon', '69000', 'Adresse 1');
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A002', 'France', 'Reims', '51100', 'Adresse 2');
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A003', 'France', 'Compiegne', '60200', 'Adresse 3');
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A004', 'France', 'Paris', '75002', 'Adresse 4');
INSERT INTO Adresse (Nb_Adresse, pays, ville, codePostal, adresse)
VALUES ('A005', 'France', 'Nice', '06100', 'Adresse 5');
COMMIT;

