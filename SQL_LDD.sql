DROP TABLE IF EXISTS Utilisateur;
DROP TABLE IF EXISTS Critique;
DROP TABLE IF EXISTS Compte_monetaire;
DROP TABLE IF EXISTS Chat;
DROP TABLE IF EXISTS Adresse;
DROP TABLE IF EXISTS Enchere;
DROP TABLE IF EXISTS Annonce;
DROP TABLE IF EXISTS Offre;
DROP TABLE IF EXISTS Demande;
DROP TABLE IF EXISTS Enchere;
DROP TABLE IF EXISTS Transaction;
DROP TABLE IF EXISTS Rubrique;
DROP TABLE IF EXISTS Produit;
DROP TABLE IF EXISTS Proposition;



CREATE TABLE Compte_monetaire(
	Nb_compte VARCHAR(20) PRIMARY KEY, 
	Montant FLOAT CHECK(Montant >= 0)
);


CREATE TABLE Adresse(
  Nb_Adresse VARCHAR(10) PRIMARY KEY,
  pays VARCHAR(10) NOT NULL,
  ville VARCHAR(20) NOT NULL,
  codePostal VARCHAR(5) NOT NULL,
  adresse text NOT NULL,
  UNIQUE (pays,ville,adresse,codePostal)
);


CREATE TABLE Utilisateur (
  ID_utilisateur VARCHAR(10) PRIMARY KEY,
  Password VARCHAR NOT NULL,
  Nom VARCHAR(50) NOT NULL, 
  Prenom VARCHAR(50) NOT NULL, 
  DateNaissance Date, 
  Sexe CHAR(1), 
  Type VARCHAR NOT NULL, 
  Description text, 
  Email VARCHAR(50) NOT NULL, 
  Credit INTEGER, 
  Adresse VARCHAR NOT NULL, 
  NumeroCompte VARCHAR NOT NULL,
  NiveauDroit INTEGER,      
  FOREIGN KEY (Adresse) REFERENCES Adresse (Nb_Adresse),
  FOREIGN KEY (NumeroCompte) REFERENCES Compte_monetaire (Nb_compte),
  CHECK (NiveauDroit BETWEEN 1 AND 3),
  CHECK (Credit BETWEEN 1 AND 5),
  CHECK (Type in ('Usager','Administrateur')),
  UNIQUE (Email),
  UNIQUE (Nom,Prenom)
) ;

CREATE TABLE Critique (
    ID_evaluateur VARCHAR(10) ,
    ID_evaluee VARCHAR(10) ,
    NiveauCritique INTEGER,
    Description text,
    FOREIGN KEY (ID_evaluateur) REFERENCES Utilisateur(ID_utilisateur),
    FOREIGN KEY (ID_evaluee) REFERENCES Utilisateur (ID_utilisateur),
    PRIMARY KEY (ID_evaluateur,ID_evaluee),
    CHECK (ID_evaluateur <> ID_evaluee),
    CHECK (NiveauCritique BETWEEN 1 AND 5)
) ;

CREATE TABLE Chat(
  Id_envoyeur VARCHAR(10),
  Id_receveur VARCHAR(10) ,
  texte text,
  temps_post Date,
  FOREIGN KEY (Id_envoyeur) REFERENCES Utilisateur (ID_utilisateur),
  FOREIGN KEY (Id_receveur) REFERENCES Utilisateur (ID_utilisateur),
  PRIMARY KEY(Id_envoyeur,Id_receveur),
  CHECK (Id_envoyeur <> Id_receveur)
);

CREATE TABLE Rubrique (
  Titre VARCHAR(100) PRIMARY KEY,
  Description text
);


CREATE TABLE Annonce (
	Nb_annonce VARCHAR(10) PRIMARY KEY, 
	Title VARCHAR(50) NOT NULL, 
	Type_activite VARCHAR(20) NOT NULL,
	Description text, 
	Temps_post date NOT NULL, 
	Status VARCHAR(20), 
  Temps_disponible INTEGER NOT NULL,
	Rubrique VARCHAR(100) NOT NULL,
	Id_utilisateur VARCHAR(10)NOT NULL,
  Nb_adresse VARCHAR(10) NOT NULL,
	FOREIGN KEY (Rubrique) REFERENCES Rubrique(Titre),
	FOREIGN KEY (Id_utilisateur) REFERENCES Utilisateur(ID_utilisateur), 
	FOREIGN KEY (Nb_adresse) REFERENCES Adresse(Nb_adresse),
  CHECK (Type_activite in ('vente', 'don', 'echange','recherche')),
  CHECK (Status in ('accepted','waiting'))
);

CREATE TABLE Transaction(
  Nb_Transtraction VARCHAR (10) PRIMARY KEY,
  Usager VARCHAR (10) NOT NULL,
  Nb_Annonce VARCHAR (10) NOT NULL,
  Type VARCHAR(20),
  Status VARCHAR(20) NOT NULL,
  Description text,
  Temps_action TIMESTAMP NOT NULL,
  Commentaire text,
  FOREIGN KEY (Usager) REFERENCES Utilisateur (ID_utilisateur),
  FOREIGN KEY (Nb_Annonce) REFERENCES Annonce (Nb_annonce),
  CHECK (Type in ('dons', 'propositon', 'enchere','echange', 'vente')),
  CHECK (Status in ('Successful', 'canceled', 'rejected')),
  UNIQUE(Usager,Nb_Annonce)
);


CREATE TABLE Enchere (
  Nb_Enchere VARCHAR(10) PRIMARY KEY,
  Prix_initial FLOAT NOT NULL,
  Prix_Actuel FLOAT NOT NULL,
  Gagneur VARCHAR (10),
  FOREIGN KEY (Nb_Enchere) REFERENCES Annonce(Nb_annonce),
  FOREIGN KEY (Gagneur) REFERENCES Utilisateur(ID_utilisateur),
  CHECK (Prix_initial >=0),
  CHECK (Prix_Actuel >=0),
  CHECK (Prix_Actuel >= Prix_Initial)
);

CREATE TABLE Offre(
  Nb_offre VARCHAR(10)PRIMARY KEY,
  prix FLOAT NOT NULL,
  FOREIGN KEY ( Nb_offre) REFERENCES Annonce(Nb_annonce),
  CHECK (prix >= 0)
);


CREATE TABLE Demande(
  Nb_demande VARCHAR(10) PRIMARY KEY,
  FOREIGN KEY (Nb_demande) REFERENCES Annonce(Nb_annonce) 
);


CREATE TABLE Produit(
  Nb_Produit VARCHAR(10) PRIMARY KEY,
  typeProduit VARCHAR,
  categorie VARCHAR,
  NomProduit VARCHAR NOT NULL,
  Description VARCHAR,
  Imagepath VARCHAR,
  Annonce VARCHAR(10) NOT NULL,
  FOREIGN KEY (Annonce) REFERENCES Annonce(Nb_annonce),
  CHECK (typeProduit in ('objet', 'service'))
);

CREATE TABLE Proposition(
  Nb_enchere VARCHAR(10) ,
  Id_utilisateur VARCHAR(10) ,
  Prix FLOAT NOT NULL,
  FOREIGN KEY (Nb_enchere) REFERENCES Enchere(Nb_Enchere),
  FOREIGN KEY (Id_utilisateur) REFERENCES Utilisateur(Id_utilisateur),
  PRIMARY KEY(Nb_enchere,Id_utilisateur),
  CHECK (Prix >=0)
);