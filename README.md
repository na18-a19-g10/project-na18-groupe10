## Brève description

Projet de NA18 pour le semestre A19 réalisé par le groupe 10 de BOIDOT Charles, LU Ziyu, MARQUIS Antoine, YANEZ ROMERO Joaquin, ZHAO Yufei.

L'objectif du projet est de réaliser une place d'échange à l'UTC. 

- Le projet pourra intégrer des biens et des services.
- Le projet pourra intégrer des actions de dons, d'échange ou de vente.
- Le projet pourra intégrer la gestion d'une monnaie interne.
- Les utilisateurs pourront déposer des offres ou des demandes, qui pourront être localisées dans le temps et l'espace.
- Les annonces pourront être classées selon des rubriques, des mots-clés...
- On peut ajouter des mécanismes d'enchère, de négociation...
- Le projet pourra intégrer un système de communication interne.
- Le projet pourra intégrer un historique des actions menées.



- Vous pouvez rechercher la note de clarification dans  le fichier `Note de Clarification.md`

- Ainsi que Modèlisation concepuelle de donnée avec UML, et vous pouvez trouver le code du format .plantuml dans le fichier `MCD.plantuml`.




![MCD](MCD.png)

- Vous pouvez rechercher le SQL (LDD) dans le fichier `SQL_LDD.sql`.

