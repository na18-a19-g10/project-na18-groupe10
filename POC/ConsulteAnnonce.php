<?php

$vHost = 'localhost';
$vPort = '5432';
$vDbname = 'nomdb';
$vUser = 'dbuser';
$vPassword = '123456';
try{
$vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vDbname", $vUser, $vPassword);
}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
if ($vConn){
    echo "connect successful"."\n"."\n";
    $sql = "SELECT title FROM Annonce";
    $resultset = $vConn->prepare($sql);
    $resultset->execute();
    echo "Traitement des donnes..."."\n"."\n";
    //Traitement des résultats
    while($annonce = $resultset->fetch(PDO::FETCH_ASSOC)) {
      echo $annonce['title']."\n";
    }
    $vConn = null;
    echo "\n"."Deconnexion de la base."."\n";
}
else {
    echo "can't connect "."\n";

}

?>
