<?php
include 'getId.php';
$vHost = 'localhost';
$vPort = '5432';
$vDbname = 'postgres';
$vUser = 'postgres';
$vPassword = '123456';
try{
$vConn = new PDO("pgsql:host=$vHost;port=$vPort;dbname=$vDbname", $vUser, $vPassword);
}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
if ($vConn){
    echo "connect successful"."\n";

}
else {
    echo "can't connect ";
}
$username = "username";
if(!isset($_COOKIE[$username])){
    echo '<script>alert("Connectez-vous, svp");location.href="login.html";</script>;';
}
else{
    $user_id = $_COOKIE[$username];
}

$annonceTitre = $annonceType = $annonceTemps = $rubriqueSelect = $rubriqueInput = $annonceMode = $annonceDes =  $rubriqueDes =  "";
$annoncePrix = 0;
if (empty($_POST["annonceTitre"])){
    echo '<script>alert("Annonce Titre est vide");location.href="creerAnnonce.php";</script>;';
}
else{
    $annonceTitre = $_POST["annonceTitre"];
}
if (!isset($_POST["annonceType"])){
    echo '<script>alert("Annonce Type est vide");location.href="creerAnnonce.php";</script>;';
}
else{
    $annonceType = $_POST["annonceType"];
}
if (empty($_POST["annonceTemps"])){
    echo '<script>alert("Annonce temps disponible est vide");location.href="creerAnnonce.php";</script>;';
}
else{
    $annonceTemps = $_POST["annonceTemps"];
}
if (empty($_POST["rubriqueSelect"])){
    if (empty($_POST["rubriqueInput"])){
        echo '<script>alert("Confirmez votre rubrique");location.href="creerAnnonce.php";</script>;';
    }
    else{
        $rubriqueInput = $_POST["rubriqueInput"];
    }
}
else{
    $rubriqueSelect = $_POST["rubriqueSelect"];
}

if (!isset($_POST["annonceMode"])){
    echo '<script>alert("Annonce Mode est vide");location.href="creerAnnonce.php";</script>;';
}
else{
    $annonceMode = $_POST["annonceMode"];
}

if(empty($_POST["annoncePrix"]) && $annonceMode != "2"){
    echo '<script>alert("Annonce prix est vide");location.href="creerAnnonce.php";</script>;';
}else if(!empty($_POST["annoncePrix"])){
    $annoncePrix = $_POST["annoncePrix"];
}

if (!empty($_POST["annonceDes"])){
    $annonceDes = $_POST["annonceDes"];
}
else{$annonceDes = null;}

if (!empty($_POST["rubriqueDes"])){
    $rubriqueDes = $_POST["rubriqueDes"];
}else{$rubriqueDes = null;}

$nomProduit = $typeProduit = $catProduit = $idProduit = $photoProduit = $disProduit = null;
if(isset($_POST["nomProduit"])){
    $nomProduit = $_POST["nomProduit"];
    if(isset($_POST["typeProduit"])){
        $typeProduit = $_POST["typeProduit"];
    }
    if(isset($_POST["catProduit"])){
        $catProduit = $_POST["catProduit"];
    }
    if(isset($_POST["photoProduit"])){
        $photoProduit = $_POST["photoProduit"];
    }
    if(isset($_POST["disProduit"])){
        $disProduit = $_POST["disProduit"];
    }
}

try{
    //insert Rubrique
    $annonceid=getid();
    $vConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $vConn->beginTransaction();
    if(!empty($annonceInput)){
        $rubrique = $annonceInput;
    }else if(empty($annonceInput)){
        $rubrique = $rubriqueSelect;
    }
    $stmtRub = $vConn->prepare("SELECT titre FROM Rubrique");
    $stmtRub->execute();
    $flag = 1;
    while($row = $stmtRub->fetch(PDO::FETCH_ASSOC)){
        if($rubrique == $row["titre"]){
            $flag = 0;
        }
    }
    if($flag==1){
        $stmt = $vConn->prepare("INSERT INTO rubrique (Titre ,Description) VALUES (?,?)");
        $stmt->bindValue(1, $rubrique);
        $stmt->bindValue(2, $annonceDes);
        $stmt->execute();
    }

   
    // insert Annonce
    $stmt1 = $vConn->prepare("INSERT INTO annonce (Nb_annonce, Title, Type_activite, Description, Temps_post, Status, Temps_disponible, Rubrique, Id_utilisateur) VALUES (?,?,?,?,?,?,?,?,?)");
    $status = 'waiting';
    $tempsPost = date("Y-m-d",time());
    $annonceTemps = strtotime($annonceTemps);
    $stmt1->bindValue(1, $annonceid);
    $stmt1->bindValue(2, $annonceTitre);
    $stmt1->bindValue(3, $annonceType);
    $stmt1->bindValue(4, $annonceDes);
    $stmt1->bindValue(5, $tempsPost);
    $stmt1->bindValue(6, $status);
    $stmt1->bindValue(7, $annonceTemps);
    $stmt1->bindValue(8, $rubrique);
    $stmt1->bindValue(9, $user_id);
    $stmt1->execute();

    if($annonceMode == "0"){
        $stmt2 = $vConn->prepare("INSERT INTO enchere (Nb_Enchere, Prix_initial, Prix_Actuel, Gagneur) VALUES (?, ?, ?, ?)");
        $stmt2->bindValue(1, $annonceid);
        $stmt2->bindValue(2, $annoncePrix);
        $stmt2->bindValue(3, $annoncePrix);
        $stmt2->bindValue(4, null);
        $stmt2->execute();
    }else if($annonceMode == "1"){
        $stmt2 = $vConn->prepare("INSERT INTO offre (Nb_offre, prix) VALUES (?, ?)");
        $stmt2->bindValue(1, $annonceid);
        $stmt2->bindValue(2, $annoncePrix);
        $stmt2->execute();
    }else if($anonceMode == "2"){
        $stmt2 = $vConn->prepare("INSERT INTO demande (Nb_demande) VALUES (?)");
        $stmt2->bindValue(1, $annonceid);
        $stmt2->execute();
    }
    $vConn->commit();

    if($nomProduit != null){
        $vConn->beginTransaction();
        $stmtProduit = $vConn->prepare("SELECT Nb_Produit FROM Produit");
        $stmtProduit->execute();
        if(!($row = $stmtProduit->fetch(PDO::FETCH_ASSOC))){
            $idProduit = "1";
        }else{
            while($row){
                $idProduit = (int)$row["Nb_Produit"] + 1;
                $row = $stmtProduit->fetch(PDO::FETCH_ASSOC);
            }
            $idProduit = (string)$idProduit;
        }
        $stmt3 = $vConn->prepare("INSERT INTO Produit (Nb_Produit, typeProduit, categorie, NomProduit, Description, Imagepath, Annonce) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $stmt3->bindValue(1, $idProduit);
        $stmt3->bindValue(2, $typeProduit);
        $stmt3->bindValue(3, $catProduit);
        $stmt3->bindValue(4, $nomProduit);
        $stmt3->bindValue(5, $disProduit);
        $stmt3->bindValue(6, $photoProduit);
        $stmt3->bindValue(7, $annonceid);
        $stmt3->execute();
        $vConn->commit();
    }
    
    echo '<script>alert("Annonce creation reusit !");location.href="homepage.html";</script>;';
}catch(PDOException $e){
    $vConn->rollBack();
    echo "Failed: " . $e->getMessage();
}
?>